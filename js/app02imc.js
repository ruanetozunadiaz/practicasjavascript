const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let altura = parseFloat(document.getElementById('txtAltura').value);
    let peso = parseFloat(document.getElementById('txtPeso').value);

    let imc = peso / (altura * altura);

    document.getElementById('txtResultado').value = imc.toFixed(2);

    mostrarNivel(imc, peso);
 });

 function mostrarNivel(imc, peso){
    let nivel = "";
    let imagen = "";
    let calorias = 0;

    let sexoMasculino = document.getElementById('sexomasculino').checked;
    let sexoFemenino = document.getElementById('sexofemenino').checked;

    let edad = parseInt(document.getElementById('txtEdad').value);

    if(sexoMasculino){
        if (imc < 18.5) {
            nivel = "Peso por debajo de lo normal";
            imagen = "01H.png";
        } else if (imc >= 18.5 && imc < 24.99) {
            nivel = "Peso saludable";
            imagen = "02H.png";
        } else if (imc >= 25 && imc < 29.99) {
            nivel = "Sobrepeso";
            imagen = "03H.png";
        } else if (imc >= 30 && imc < 34.99) {
            nivel = "Obesidad Tipo I";
            imagen = "04H.png";
        } else if (imc >= 35 && imc < 39.99) {
            nivel = "Obesidad Tipo II";
            imagen = "05H.png";
        } else {
            nivel = "Obesidad Tipo III";
            imagen = "06H.png";
        }
    }

    if(sexoFemenino){
        if (imc < 18.5) {
            nivel = "Peso por debajo de lo normal";
            imagen = "01M.png";
        } else if (imc >= 18.5 && imc < 24.99) {
            nivel = "Peso saludable";
            imagen = "02M.png";
        } else if (imc >= 25 && imc < 29.99) {
            nivel = "Sobrepeso";
            imagen = "03M.png";
        } else if (imc >= 30 && imc < 34.99) {
            nivel = "Obesidad Tipo I";
            imagen = "04M.png";
        } else if (imc >= 35 && imc < 39.99) {
            nivel = "Obesidad Tipo II";
            imagen = "05M.png";
        } else {
            nivel = "Obesidad Tipo III";
            imagen = "06M.png";
        }
    }

    document.getElementById('txtNivel').value = nivel;

    mostrarImagen(imagen);

    if(sexoMasculino){
        if (edad >= 10 && edad <= 17) {
            calorias = (17.686 * peso) + 658.2;
        } else if (edad >= 18 && edad <= 29) {
            calorias = (15.057 * peso) + 692.2;
        } else if (edad >= 30 && edad <= 59) {
            calorias = (11.472 * peso) + 873.1;
        } else {
            calorias = (11.711 * peso) + 587.7;
        }
    }

    if(sexoFemenino){
        if (edad >= 10 && edad <= 17) {
            calorias = (13.384 * peso) + 692.6;
        } else if (edad >= 18 && edad <= 29) {
            calorias = (14.818 * peso) + 486.6;
        } else if (edad >= 30 && edad <= 59) {
            calorias = (8.126 * peso) + 845.6;
        } else {
            calorias = (9.082 * peso) + 658.5;
        }
    }

    document.getElementById('txtCalorias').value = calorias.toFixed(0);
 }

function mostrarImagen(imagen) {
    let rutaImagen = "/img/" + imagen;
    document.getElementById('imagenNivel').src = rutaImagen;
}

