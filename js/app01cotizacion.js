const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let valorAuto = document.getElementById('txtValorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;

    // Hacer los calculos
    let pagoInicial = valorAuto * (porcentaje / 100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin / plazo;

    // Mostrar los datos
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazo;
});

btnLimpiar.addEventListener('click', function(){
    txtValorAuto.value = '';
    txtPorcentaje.value = '';
    txtPagoInicial.value = '';
    txtTotalFin.value = '';
    txtPagoMensual.value = '';
});