const btnMostrarTabla = document.getElementById('btnMostrarTabla');
const valor = document.getElementById('opcionTabla');

btnMostrarTabla.addEventListener('click', function () {
    const numero = valor.value;
    
    imagenprimer.innerHTML = "";
    imagenpor.innerHTML = "";
    imagensegundo.innerHTML = "";
    imagenigual.innerHTML = "";
    imagenresultado.innerHTML = "";
    imagenresultadodos.innerHTML = "";
    
    if (numero == 1) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/1.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            const img5 = document.createElement('img');
            img5.src = "/img/"+x+".png";
            imagenresultado.appendChild(img5);
        }
    } else if (numero == 2) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/2.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 2 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }        
    } else if (numero == 3) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/3.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 3 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 4) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/4.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 4 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        } 
    } else if (numero == 5) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/5.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 5 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 6) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/6.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 6 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 7) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/7.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 7 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 8) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/8.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 8 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 9) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/9.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 9 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } else if (numero == 10) {
        for (let x = 1; x < 11; x++) {
            const img1 = document.createElement('img');
            img1.src = "/img/10.png";
            imagenprimer.appendChild(img1);
            const img2 = document.createElement('img');
            img2.src = "/img/x.png";
            imagenpor.appendChild(img2);
            const img3 = document.createElement('img');
            img3.src = "/img/"+x+".png";
            imagensegundo.appendChild(img3);
            const img4 = document.createElement('img');
            img4.src = "/img/=.png";
            imagenigual.appendChild(img4);
            let primerdigito = 0;
            let segundodigito = 0;
            let total = 0;
            total = 10 * x;
            primerdigito = Math.floor(total/10);
            segundodigito = total % 10;
            const img5 = document.createElement('img');
            img5.src = "/img/"+primerdigito+".png";
            imagenresultado.appendChild(img5);
            const img6 = document.createElement('img');
            img6.src = "/img/"+segundodigito+".png";
            imagenresultadodos.appendChild(img6);
        }
    } 
});