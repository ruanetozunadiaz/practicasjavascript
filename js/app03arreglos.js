// Acción del botón

const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function () {
    let cantidadNumeros = parseInt(document.getElementById('txtNumGenerar').value);

    const numAleatorios = document.getElementById('slctNumeros');
    while (numAleatorios.firstChild) {
        numAleatorios.removeChild(numAleatorios.firstChild);
    }

    const arregloAleatorio = random(cantidadNumeros);

    promedio(arregloAleatorio);
    mayor(arregloAleatorio);
    menor(arregloAleatorio);
    simetrico(arregloAleatorio);
});

// Función que genera numeros randoms dentro de un arreglo (iniciando en la posición 0)

function random(cantidadNumeros) {
    const numAleatorios = document.getElementById('slctNumeros');

    let arregloAleatorio = [];

    for (let x = 0; x < cantidadNumeros; ++x) {
        arregloAleatorio[x] = (Math.random() * 1000).toFixed(0);

        let option = document.createElement('option');
        option.value = arregloAleatorio[x];
        option.innerHTML = arregloAleatorio[x];
        numAleatorios.appendChild(option);
    }

    return arregloAleatorio;
}

// Función para sacer el promedio mediante operaciones tomando en cuenta el arreglo

function promedio(arregloAleatorio) {
    let promedio = 0;
    let suma = 0;

    for (let x = 0; x < arregloAleatorio.length; ++x) {
        suma = suma + parseInt(arregloAleatorio[x]);
    }

    promedio = suma / arregloAleatorio.length;

    document.getElementById('txtPromedio').value = promedio.toFixed(2);
}

// Funcion donde por medio de decisiones sale el numero mayor y su posición

function mayor(arregloAleatorio) {
    let mayor = arregloAleatorio[0];
    let posicionMayor = 0;

    for (let x = 0; x < arregloAleatorio.length; ++x) {
        if (arregloAleatorio[x] >= mayor) {
            mayor = arregloAleatorio[x];
            posicionMayor = x;
        }
    }

    document.getElementById('txtMayor').value = mayor;
    document.getElementById('txtPosicionMayor').value = ("[ " + posicionMayor + " ]");
}

// Funcion donde por medio de decisiones sale el numero menor y su posición

function menor(arregloAleatorio) {
    let menor = arregloAleatorio[0];
    let posicionMenor = 0;

    for (let x = 0; x < arregloAleatorio.length; ++x) {
        if (arregloAleatorio[x] < menor) {
            menor = arregloAleatorio[x];
            posicionMenor = x;
        }
    }

    document.getElementById('txtMenor').value = menor;
    document.getElementById('txtPosicionMenor').value = ("[ " + posicionMenor + " ]");
}

// Función que decide si el porcentaje es simetrico y tambien hace el porcentaje de pares e impares

function simetrico(arregloAleatorio) {
    const cantidadNumeros = arregloAleatorio.length;
    let pares = 0;
    let impares = 0;

    for (let x = 0; x < cantidadNumeros; x++) {
        if (arregloAleatorio[x] % 2 === 0) {
            pares++;
        } else {
            impares++;
        }
    }

    const porcentajePares = (pares / cantidadNumeros) * 100;
    const porcentajeImpares = (impares / cantidadNumeros) * 100;

    document.getElementById('txtPorcentajePares').value = porcentajePares.toFixed(2);
    document.getElementById('txtPorcentajeImpares').value = porcentajeImpares.toFixed(2);

    document.getElementById('txtSimetrico').value = "";

    if (porcentajePares <= 20.00 || porcentajeImpares <= 20.00) {
        document.getElementById('txtSimetrico').value = "No, no es simétrico";
    } else {
        document.getElementById('txtSimetrico').value = "Sí, es simétrico";
    }
}